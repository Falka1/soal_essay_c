<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "quiz";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// sql to create table
$sql = "CREATE TABLE costumers (
id INT(25) AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(255) NOT NULL,
email VARCHAR(255) NOT NULL,
password VARCHAR(255)
)";

$sql2 = "CREATE TABLE orders (
    id INT(25) AUTO_INCREMENT PRIMARY KEY,
    amount VARCHAR(255) NOT NULL,
    costumer_id INT
    )";

$sql3 = "ALTER TABLE orders ADD FOREIGN KEY (costumer_id) REFERENCES costumers(id)";


if ($conn->query($sql) === TRUE) {
  echo "Table costumers created successfully<br>";
} else {
  echo "Error creating table: " . $conn->error;
}
if ($conn->query($sql2) === TRUE) {
    echo "Table orders created successfully<br>";
  } else {
    echo "Error creating table: " . $conn->error;
  }
if ($conn->query($sql3) === TRUE) {
    echo "alter table successfully<br>";
} else {
    echo "Error creating table: " . $conn->error;
}

$conn->close();
?>